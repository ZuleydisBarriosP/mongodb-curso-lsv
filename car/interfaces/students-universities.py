from dataclasses import asdict, dataclass
from datetime import datetime


@dataclass
class Universities:
    uuid: str
    label: str
    value: str

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass
class StudentSummary:
    uuid: str  # UUID
    first_name: str
    last_name: str
    document_type: str
    document_number: str
    university_id: Universities  # puede llevar uuid o id de universities
    created_at: datetime
    modified_at: datetime

    def to_dict(self) -> dict:
        return asdict(self)

    @property
    def get_university(self):
        return self.university_id.label
