from bson.objectid import ObjectId

from car.databases.client import MongoLsv  # noqa

mongo_lsv = MongoLsv()

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="mongo_lsv",
        collection="students",
        record={
            "first_name": "Valentin",
            "last_name": "Castillo",
            "document_number": "700000011",
            "document_type": "CC",
            "university_id": "62ad495a4f2f9212cf44be8e",
            "created_at": "2020-08-01",
            "modified_at": "2020-08-02",
        },
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="mongo_lsv", collection="students"
    )
)

'''
Codigo para crear un nuevo registro en la colección universities
'''

