from bson.objectid import ObjectId
from car.databases.client import MongoLsv  # noqa



mongo_lsv = MongoLsv()

print(
    mongo_lsv.delete_record_in_collection(
        db_name="mongo_lsv",
        collection="students",
        record_id="62ad5ad6a1f46d83af546837",
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="mongo_lsv", collection="students"
    )
)

'''
Codigo para eliminar un registro en la coleccion universities
'''
