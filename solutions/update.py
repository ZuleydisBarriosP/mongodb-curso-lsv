from bson.objectid import ObjectId

from car.databases.client import MongoLsv  # noqa

mongo_lsv = MongoLsv()

print(
    mongo_lsv.update_record_in_collection(
        db_name="mongo_lsv",
        collection="students",
        record_query={"_id": ObjectId("62ad59910a3c424323d98091")},
        record_new_value={"first_name": "antonio"},
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="mongo_lsv", collection="students"
    )
)

'''
Codigo para actualizar un registro en la coleccion universities
'''
